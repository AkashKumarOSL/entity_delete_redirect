<?php

namespace Drupal\entity_delete_redirect\Validate;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class EntityDeleteRedirectConfigFormValidate.
 *
 * @package Drupal\entity_delete_redirect\Validate
 */
class EntityDeleteRedirectConfigFormValidate {

  /**
   * Validates given element.
   *
   * @param array $form
   *   The form to process.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public static function validate(array &$form, FormStateInterface $formState) {
    /*
     * @todo
     *  add form validation
     */

  }

}
