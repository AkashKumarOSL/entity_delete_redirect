<?php

namespace Drupal\entity_delete_redirect\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface as ContainerInterfaceAlias;

/**
 * Class EntityDeleteRedirectConfigForm.
 *
 * @package Drupal\entity_delete_redirect\Form
 */
class EntityDeleteRedirectConfigForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Node configuration object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeConfigStorage;

  /**
   * EntityDeleteRedirectConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory object.
   * @param \Drupal\Core\Entity\EntityStorageInterface $nodeConfigStorage
   *   Node configuration object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityStorageInterface $nodeConfigStorage) {
    parent::__construct($config_factory);
    $this->nodeConfigStorage = $nodeConfigStorage;
  }

  /**
   * Static function create.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container object.
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\entity_delete_redirect\Form\EntityDeleteRedirectConfigForm
   *   Returns services.
   */
  public static function create(ContainerInterfaceAlias $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('node_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_delete_redirect.admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_delete_redirect.admin_settings_form',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Loading config file
    $config = $this->config('entity_delete_redirect.admin_settings_form')
      ->get('entity_admin_form_settings');
    
    $options = [
      0 => $this->t('Disable'),
      1 => $this->t('Enable'),
    ];

    $elemValidate = [
      [
        'Drupal\entity_delete_redirect\Validate\EntityDeleteRedirectElemPathValidate',
        'validate',
      ],
    ];

    $formValidate = [
      [
        'Drupal\entity_delete_redirect\Validate\EntityDeleteRedirectConfigFormValidate',
        'validate',
      ],
    ];

    $form['edr_check'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Entity Delete Redirect'),
      '#default_value' => isset($config['edr_check']) ? $config['edr_check'] : 0,
      '#options'       => $options,
      '#description'   => $this->t('When disabled, redirection rules will not apply.'),
      '#required'      => TRUE,
    ];
    
    // Get all entites type. 
    $entity_types = \Drupal::entityManager()->getEntityTypeLabels(TRUE);
    // Name of content entity.
    $content_entity = array_keys($entity_types['Content']);
    // Load all content entity values.
    $entity = \Drupal::entityManager()->getAllBundleInfo();
    $all_entity = array_keys($entity);

    foreach($content_entity as $value) {
      if(in_array($value, $all_entity)) {
        $content_bundle[$value] = $entity[$value];
      }
    }

    foreach($content_bundle as $content_key=>$content_value) {
      foreach($content_value as $content_name=>$content_array) {
        foreach($content_array as $label=>$name) {
          if(is_object($name)) {
            $content_entity_arr[$content_key][$content_name][] = $name->getUntranslatedString();
          }
          else {
            $content_entity_arr[$content_key][$content_name][] = $name;
          }
        }
      }
    }
    
    foreach ($content_entity_arr as $keys => $values) {
      // User entity is not supported. 
      if($keys != 'user') {
      $form[$keys] = [
        '#type'        => 'fieldset',
        '#title'       => $this->t('Enable Redirect For '.$keys.' Type'),
        '#collapsible' => FALSE,
        '#collapsed'   => FALSE,
        '#tree'        => TRUE,
        '#states'      => [
          'visible' => [
            ':input[name=edr_check]' => [
              'value' => 1,
            ],
          ],
        ],
      ];

      foreach ($values as $label => $val) {
        $form[$keys][$label]['is_enabled'] = [
          '#type'          => 'checkbox',
          '#title'         => reset($val),
          '#default_value' => isset($config[$keys][$label]['is_enabled']) ? $config[$keys][$label]['is_enabled'] : '',
        ];

        $form[$keys][$label]['redirect_' . $keys . '_settings'] = [
          '#type'        => 'fieldset',
          '#title'       => $this->t('Redirect URL settings'),
          '#collapsible' => FALSE,
          '#collapsed'   => FALSE,
          '#parents'     => [$keys, $label],
          '#states'      => [
            'visible' => [
              ':input[name='.$keys.'\[' . $label . '\]\[is_enabled\]]' => [
                'checked' => TRUE,
              ],
            ],
          ],
        ];

        $form[$keys][$label]['redirect_' . $keys . '_settings']['redirect'] = [
          '#type'             => 'textfield',
          '#title'            => $this->t('Redirect URL'),
          '#default_value'    => isset($config[$keys][$label]['redirect']) ? $config[$keys][$label]['redirect'] : '',
          '#description'      => $this->t('Example: /node'),
          '#element_validate' => $elemValidate,
          '#size'             => 45,
          '#maxlength'        => 128,
        ];
      }
      $form['#validate'] = $formValidate;
    }}

    $form['edr_lang'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable language support (alpha)'),
      '#default_value' => isset($config['edr_lang']) ? $config['edr_lang'] : 0,
      '#options'       => $options,
      '#description'   => $this->t('When enabled, redirection will occur with current language prefix.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Saving settings.
    $this->config('entity_delete_redirect.admin_settings_form')
      ->set('entity_admin_form_settings', $values)
      ->save();
    
    parent::submitForm($form, $form_state);
  }

}